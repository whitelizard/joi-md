import joi from '@hapi/joi';
import { test } from 'tap';
import { joiObjectToCells, rowsToMdTable, joiKeyToCells } from '../src';

test('Random object 1', async t => {
  const object1 = joi.object().keys({
    id: joi.string().description('Text'),
    active: joi.boolean(),
    ttl: joi.date(),
    cooldown: joi.number().min(0),
    onLoad: joi.array(),
    process: joi
      .array()
      .items(
        joi
          .alternatives()
          .try(joi.object().unknown(), joi.array().items(joi.string().required(), joi.any())),
      )
      .description('Text'),
    condition: joi.array().items(joi.string().required(), joi.any()),
  });
  const dm = object1.describe();
  console.log(JSON.stringify(dm, undefined, 2));
  const rows = joiObjectToCells(dm);
  console.log(rowsToMdTable(rows));
});

test('Random object 2', async t => {
  const object1 = joi
    .object()
    .meta({ name: 'AssetType', super: 'V' })
    .unknown()
    .keys({
      id: joi
        .string()
        .required()
        .meta({ unique: true }),
      rpcs: joi.object().pattern(joi.any(), joi.string()),
      items: joi.array().items(
        joi
          .object()
          .unknown()
          .keys({
            props: joi.object({
              title: joi.alternatives().try(
                joi.string(),
                joi
                  .array()
                  .min(1)
                  .items(joi.string().required(), joi.any()),
              ),
              menuItems: joi.array().items(
                joi.object().keys({
                  label: joi
                    .alternatives()
                    .try(
                      joi.string(),
                      joi
                        .array()
                        .min(1)
                        .items(joi.string().required(), joi.any()),
                    )
                    .required(),
                }),
              ),
            }),
          }),
      ),
      avatar: joi.alternatives().try(joi.string(), joi.func()),
    });
  const dm = object1.describe();
  console.log(JSON.stringify(dm, undefined, 2));
  const rows = joiObjectToCells(dm);
  console.log(rowsToMdTable(rows));
});

test('Test object', async t => {
  const miniMALcmd = joi
    .array()
    .min(1)
    .items(joi.string().required(), joi.any());

  const miniMALBlock = joi.array().items(joi.object().unknown(), miniMALcmd);

  const Rule = joi
    .object()
    .unknown()
    .keys({
      id: joi.string().description('Identifier for this particular rule.'),
      active: joi
        .boolean()
        .default(false)
        // .required()
        .description('If the rule is active or not. An inactive rule is not run at all.'),
      // actuator: joi
      //   .string()
      //   .default('backend')
      //   .description('ID of where the rule should be loaded and run.'),
      ttl: joi.date().description('At this time (ISO timestamp) the rule will be set to inactive.'),
      cooldown: joi
        .number()
        .min(0)
        .description("A rule can't be triggered again unless this number of seconds has passed."),
      onLoad: miniMALBlock.description('MiniMAL command block to run when rule is loaded.'),
      process: miniMALBlock.description(
        'MiniMAL command block to run when rule is triggeed, before condition.',
      ),
      condition: miniMALcmd.description(
        'MiniMAL command to check if rule should execute (state to flipped, run actions etc).',
      ),
      actions: miniMALBlock.description(
        'MiniMAL command block to execute when condition is true (& not in flipped state).',
      ),
      resetCondition: miniMALcmd.description(
        'MiniMAL command to check if rule should reset, if it is in flipped state.',
      ),
      resetActions: miniMALBlock.description(
        'MiniMAL command block to execute when resetCondition is true.',
      ),
    });
  const dm = Rule.describe();
  console.log(JSON.stringify(dm, undefined, 2));
  // const rows = joiKeyToCells([], ['', dm]);
  const rows = joiObjectToCells(dm);
  console.log(rowsToMdTable(rows));
});
