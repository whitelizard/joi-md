import * as R from 'ramda';

export const joiRulesToCell = ({ name, args: { limit } = {} }) => {
  // console.log('Rule:', name, limit);
  const symbol = Object.is(name, 'min') ? '>=' : (Object.is(name, 'max') && '<=') || '<>';
  return limit !== undefined ? `${symbol}${limit}` : '';
};

const tableHead = ['path', 'type', 'presence', 'description', 'default', 'conforms'];

// const transformIndex = R.curry((tfm, arr) => R.evolve({ [] }))
const idxToAlpha = x => String.fromCharCode(97 + x);

const idxsToAlpha = R.map(idxToAlpha);

export const joiKeyToCells = R.curry(
  (
    acc,
    [
      path,
      {
        flags: { description = '', presence, default: def } = {},
        type = 'unknown',
        matches = [],
        rules = [],
        items = [],
        keys = {},
      } = {},
    ],
  ) => {
    console.log('Processing:', path);
    const allRules = R.map(joiRulesToCell)(rules).join(', ');
    const alts = Object.is(type, 'alternatives');

    const rows = [
      [
        `${path}`,
        type,
        presence ? 'required' : 'optional',
        description,
        def !== undefined ? `${def}` : '',
        allRules,
      ],
    ];

    console.log('In', path, 'processing matches:', matches);
    const accWithMatches = alts
      ? R.compose(
          R.reduce(joiKeyToCells, R.concat(acc)(rows)),
          R.addIndex(R.map)(
            // - wrap the 1st argument (the value) in an array
            // - mix 2nd argument (loop index) together with path array's last element
            // - then concat flipped (so that result is again a pair)
            R.useWith(R.flip(R.concat), [
              R.of,
              R.o(R.of, x => path),
              //   R.evolve({
              //     [path.length - 1]: str => `${str}${idxToAlpha(x)}`,
              //   })(path),
              // ),
            ]),
          ),
          R.pluck('schema'),
        )(matches)
      : R.concat(acc)(rows);

    console.log('In', path, 'processing items:', items);
    const accWithItems = R.compose(
      R.reduce(joiKeyToCells, accWithMatches),
      R.map(R.compose(R.prepend(`${path}[x]`), R.of)),
      // R.evolve({ [path.length - 1]: str => `${str}[` })(path)
    )(items);

    console.log('In', path, 'processing keys:', keys);
    const accWithKeys = R.compose(
      R.reduce(joiKeyToCells, accWithItems),
      R.map(([key, val]) => [
        `${path}.${key}`,
        // R.evolve({
        //   [path.length - 1]: str => `${str}.${key}`,
        // })(path),
        val,
      ]),
      R.toPairs,
      // R.evolve({ [path.length - 1]: str => `${str}[` })(path)
    )(keys);

    return accWithKeys;
  },
);

export const joiObjectToCells = R.compose(
  R.reduce(joiKeyToCells, []),
  // R.tap(console.log),
  R.map(([key, val]) => [key, val]),
  R.toPairs,
  R.propOr({}, 'keys'),
);

export const rowsToMdTable = rows => {
  return R.compose(
    R.join('\n'),
    R.prepend(`| ${tableHead.join(' | ')} |`),
    R.prepend(`| ${tableHead.map(R.always('---')).join(' | ')} |`),
    R.map(row => `| ${row.join(' | ')} |`),
  )(rows);
};
